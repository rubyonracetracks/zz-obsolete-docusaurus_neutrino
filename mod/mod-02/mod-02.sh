#!/bin/bash

# AGENDA:
# * Renaming the "docs-examples-from-docusaurus" directory as "docs"
# * Renaming the "website/blog-examples-from-docusaurus" directory as "website/blog"
# * Not renaming these directories leads to error messages when you try to run the server.

echo '##################################'
echo 'Chapter 2: Renaming Subdirectories'
echo '##################################'

echo '-------------------------------------'
echo 'mv docs-examples-from-docusaurus docs'
mv docs-examples-from-docusaurus docs
wait

echo '------------------------------------'
echo 'rm -rf docs-examples-from-docusaurus'
rm -rf docs-examples-from-docusaurus

echo '-----------------------------------------------------'
echo 'mv website/blog-examples-from-docusaurus website/blog'
mv website/blog-examples-from-docusaurus website/blog
wait

echo '--------------------------------------------'
echo 'rm -rf website/blog-examples-from-docusaurus'
rm -rf website/blog-examples-from-docusaurus

git add .
git commit -m "Renamed the docs-examples-from-docusaurus and website/blog-examples-from-docusaurus subdirectories"
